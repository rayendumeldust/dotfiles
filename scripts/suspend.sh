#!/bin/bash

# This script suspends and locks the computer
# pm-suspend needs NOPASSWD rights

SUSPEND="sudo pm-suspend"
WALLPAPER="/home/freddy/Pictures/wallpaper/lockscreen-schienen.png"

i3lock -e -i ${WALLPAPER} & ${SUSPEND}
