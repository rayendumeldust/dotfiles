#!/bin/bash

set -eu

stow -v -t $HOME\
    bash\
    tmux\
    vim\
    nvim\
    i3\
    xresources\
    ipython\
    gtk2\
    gtk3\
    tmuxinator\
    alacritty\
    git\
    sway\
    kanshi\
    waybar\
    gammastep\
    swayidle\

# Create cache folders
mkdir -p ~/.cache/vim/
